package com.nbodev.newrelic.controller;

import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.annotation.Timed;

@RestController
public class DemoController {

	@Timed(value = "greetings", longTask = true)
	@GetMapping("/greetings")
	public ResponseEntity<String> greetings() throws InterruptedException {
		Thread.sleep(1_000L * ThreadLocalRandom.current().nextInt(5, 10));
		return new ResponseEntity<>("Greetings, time is: " + LocalDateTime.now(), HttpStatus.OK);
	}
}
