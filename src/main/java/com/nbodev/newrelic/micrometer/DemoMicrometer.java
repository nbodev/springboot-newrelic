package com.nbodev.newrelic.micrometer;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

/**
 * Example of Micrometer metrics, those are refreshed with random values.
 * <p>
 * Each fixedRate seconds we refresh the values
 */
@Component
public class DemoMicrometer {

	private static final Logger LOGGER = LoggerFactory.getLogger(DemoMicrometer.class);

	// counter: keeps incrementing all the time
	private final Counter demoCounter;

	// gauge: up and down
	private final AtomicInteger demoGauge;

	public DemoMicrometer(MeterRegistry meterRegistry) {
		LOGGER.info("Initializing metrics.");
		this.demoCounter = meterRegistry.counter("demo.counter");
		this.demoGauge = meterRegistry.gauge("demo.gauge", new AtomicInteger(0));
		LOGGER.info("Metrics initialized.");

	}

	@Scheduled(fixedRate = 5000)
	public void generateRandomMetricsData() {
		demoGauge.set(ThreadLocalRandom.current().nextInt(0, 500));
		LOGGER.info("demo.gauge is now set to: [{}]", demoGauge.get());

		demoCounter.increment(ThreadLocalRandom.current().nextDouble(1, 10) * ThreadLocalRandom.current().nextInt(-1, 2));
		LOGGER.info("demo.counter is now set to: [{}]", demoCounter.count());

	}

}
