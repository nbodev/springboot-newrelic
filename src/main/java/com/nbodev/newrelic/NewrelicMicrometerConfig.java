package com.nbodev.newrelic;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.CompositeMeterRegistryAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.newrelic.telemetry.micrometer.NewRelicRegistry;
import com.newrelic.telemetry.micrometer.NewRelicRegistryConfig;

import io.micrometer.core.instrument.util.NamedThreadFactory;

@Configuration
@AutoConfigureBefore({ CompositeMeterRegistryAutoConfiguration.class, SimpleMetricsExportAutoConfiguration.class })
@AutoConfigureAfter(MetricsAutoConfiguration.class)
@ConditionalOnClass(NewRelicRegistry.class)
public class NewrelicMicrometerConfig {

	@Value("${newrelic.enabled}")
	private boolean enabled;

	@Value("${newrelic.apiKey}")
	private String apiKey;

	@Value("${newrelic.serviceName}")
	private String serviceName;

	@Bean
	public NewRelicRegistryConfig newRelicConfig() {
		return new NewRelicRegistryConfig() {

			@Override
			public String get(String key) {
				return null;
			}

			@Override
			public boolean enabled() {
				return enabled;
			}

			@Override
			public String apiKey() {
				return apiKey;
			}

			@Override
			public Duration step() {
				return Duration.ofSeconds(5);
			}

			@Override
			public String serviceName() {
				return serviceName;
			}

		};
	}

	@Bean
	public NewRelicRegistry newRelicMeterRegistry(NewRelicRegistryConfig config) {
		final var newRelicRegistry = NewRelicRegistry.builder(config).build();
		// you can use add extra filtering by using the following:
		// newRelicRegistry.config().meterFilter(MeterFilter.
		newRelicRegistry.start(new NamedThreadFactory("NewRelicRegistry"));
		return newRelicRegistry;
	}
}
