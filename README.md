# Monitoring Spring Boot application with New Relic One

## Introduction
- this project shows how metrics can be pushed from a Spring Boot application to New Relic One platform. 

## New Relic One
- go to `https://newrelic.com/platform` and sign in.
- you will need an insert key so your metrics can be pushed to New Relic One platform, this key will be used as a value of this argument `-Dnewrelic.apiKey=` in our example.

## Run the fat jar
- compile with `mvn clean install`
- run `java -Dnewrelic.enabled=true -Dnewrelic.apiKey=<your api insert key here> -Dnewrelic.serviceName=MySpringBootApp -jar ./target/newrelic-0.0.1-SNAPSHOT.jar`
- the arguments above are defined in the project class `NewrelicMicrometerConfig`


## Run in the IDE
- run the `NewrelicApplication` class and use the 3 arguments `-Dnewrelic.enabled=true -Dnewrelic.apiKey=<your api insert key here> -Dnewrelic.serviceName=MySpringBootApp`

## Metrics
- the metrrics are defined in the `DemoMicrometer` class.
- the application `MySpringBootApp` will appear under `Explorer -> Services-OpenTelemetry` in the New Relic One platform, you can build dashboards to which you add the metrics.
- metrics are visible under `Metrics explorer`, in this example they are named `demo.counter` and `demo.gauge`.

## Increase New Relic Logs
- this is done in the `logback-spring.xml` file:

```
<logger name="com.newrelic.telemetry" level="TRACE" />
```

## Links 
- https://newrelic.com/blog/how-to-relic/how-to-monitor-spring-boot-applications-using-micrometer-metrics
- https://discuss.newrelic.com
- https://stackoverflow.com/questions/69694779/integrate-new-relic-with-spring-boot-using-micrometer
- https://github.com/arpan-banerjee7/springboot-micrometer-newrelic-demo
- https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#actuator.metrics.supported.timed-annotation
- https://ordina-jworks.github.io/microservices/2017/09/17/monitoring-your-microservices-with-micrometer.html